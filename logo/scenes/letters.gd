extends Node2D

func hide_letters():
	$O0.visible = false
	$O1.visible = false
	$S.visible = false

func show_next_letter():
	if $O0.visible == false:
		$O0.visible = true
		return
	if $S.visible == false:
		$S.visible = true
		return
	if $O1.visible == false:
		$O1.visible = true
		return

func _ready():
	hide_letters()
