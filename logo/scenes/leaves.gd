extends Node2D
tool

var _frame_counter: FrameCounter
var _frame_timer_animation: FrameTimer


func start_animating(frame_counter: FrameCounter):
	self._frame_counter = frame_counter
	
	for child in self.get_children():
		child.start_animating(self._frame_counter)

