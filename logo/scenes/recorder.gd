extends Node2D
const FrameCounter = preload("res://canon/classes/frame_counter.gd")
const FrameTimer = preload("res://canon/classes/frame_timer.gd")

export(float) var duration = 6
export(int) var framerate = 60

var _frame_counter: FrameCounter
var _frame_timer_recording: FrameTimer
var _frame_timer_presentation: FrameTimer

func _ready():
	self._frame_counter = FrameCounter.new(self.framerate)
	self._frame_timer_recording = FrameTimer.new(self._frame_counter)
	self._frame_timer_recording.connect("timer_complete", self, "_on_frame_timer_recording_timer_complete")
	self._frame_timer_recording.start(self.duration)
	
	$CrtOverlay.start_animating(self._frame_counter)
	$Viewport/TextAnimator.start_animating(self._frame_counter)
	$Viewport/Leaves.start_animating(self._frame_counter)

func _process(delta):
	self._frame_counter.increment()

func _on_frame_timer_recording_timer_complete():
	get_tree().quit()

func _on_TextAnimator_animation_ended():
	$Viewport/Leaves.visible = true
