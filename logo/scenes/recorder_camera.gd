extends Camera2D

func get_this_path():
	var dir = "C:/GIT_WORKING_FOLDER/render/"
	return dir

func _ready() -> void:
	if OS.has_feature("standalone"):
		get_viewport().msaa = Viewport.MSAA_2X
		var directory: = Directory.new()
		directory.make_dir(get_this_path())

func _process(delta: float) -> void:
	if OS.has_feature("standalone"):
		# The first frame is always black, there's no point in saving it
		if Engine.get_frames_drawn() == 0:
			return

		print(
				"Rendering frame {frame}...".format({
					frame = Engine.get_frames_drawn(),
				})
		)
		get_viewport().set_clear_mode(Viewport.CLEAR_MODE_ONLY_NEXT_FRAME)

		var image := get_viewport().get_texture().get_data()

		# The viewport must be flipped to match the rendered window
		image.flip_y()

		var error := image.save_png(get_this_path() + str(Engine.get_frames_drawn()) + ".png")
		
		if error != 0:
			print("Error:")
			print(error)
