extends CanvasLayer

export(ViewportTexture) var viewport_texture

var _frame_counter: FrameCounter

func _ready():
	$Effect.rect_size = get_viewport().size
	
func start_animating(frame_counter: FrameCounter):
	self._frame_counter = frame_counter
	self._frame_counter.connect("counter_changed", self, "_on_frame_counter_counter_changed")

func _on_frame_counter_counter_changed(new_num_frames, new_num_seconds):
	$Effect.material.set_shader_param("viewport_texture", viewport_texture)
	$Effect.material.set_shader_param("anim_time", new_num_seconds)
