extends CanvasLayer

export(float) var fade_duration = 1.66

func _ready():
	$FadeToBlackTween.interpolate_method(self, "set_fade", 0.0, 1.0, fade_duration, Tween.TRANS_LINEAR, Tween.EASE_IN)


func start_fading():
	$Black.visible = true
	$FadeToBlackTween.start()

func set_fade(value):
	$Black.color.a = value
