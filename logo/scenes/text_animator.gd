extends Node2D
const FrameTimer = preload("res://canon/classes/frame_timer.gd")

export(float) var start_delay = 0.50
export(float) var o0_delay = 0.66
export(float) var s_delay = 0.33
export(float) var o1_delay = 1.0

signal animation_ended

enum {
	eUninitialized
	eWaitingForO0,
	eWaitingForS,
	eWaitingForO1,
	eWaitingForWord,
	eDone
}

var _current_state = eUninitialized
var _frame_counter: FrameCounter
var _frame_timer_animation: FrameTimer

func start_animating(frame_counter: FrameCounter):
	$Word.visible = false
	$Letters.hide_letters()
	$Cursor.reset_cursor()
	
	self._frame_counter = frame_counter
	self._frame_timer_animation = FrameTimer.new(self._frame_counter)
	self._frame_timer_animation.connect("timer_complete", self, "_on_frame_timer_animation_timer_complete")
	$Cursor.start_blinking(self._frame_counter)
	
	_process_sequence()
	
func _process_sequence():
	if _current_state == eUninitialized:
		_current_state = eWaitingForO0
		self._frame_timer_animation.start(start_delay)
	elif _current_state == eWaitingForO0:
		$Letters.show_next_letter()
		$Cursor.move_cursor()
		self._frame_timer_animation.start(o0_delay)
		_current_state = eWaitingForS
	elif _current_state == eWaitingForS:
		$Letters.show_next_letter()
		$Cursor.move_cursor()
		self._frame_timer_animation.start(s_delay)
		_current_state = eWaitingForO1
	elif _current_state == eWaitingForO1:
		$Letters.show_next_letter()
		$Cursor.move_cursor()
		self._frame_timer_animation.start(o1_delay)
		_current_state = eWaitingForWord
	elif _current_state == eWaitingForWord:
		$Cursor.stop_blinking()
		$Cursor.hide_cursor()
		$Word.visible = true
		emit_signal("animation_ended")
		_current_state == eDone

func _on_frame_timer_animation_timer_complete():
	_process_sequence()
