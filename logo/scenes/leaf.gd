extends Sprite
const FrameTimer = preload("res://canon/classes/frame_timer.gd")

var _frame_counter: FrameCounter
var _frame_timer_lifetime: FrameTimer

export(Vector2) var initial_velocity = Vector2(3, 3)
export(float) var rotate_speed = 0.05
export(int) var num_rotate_angles = 10

export(float) var lifetime = 1

var _start_frame: int = 0
onready var _velocity: Vector2 = initial_velocity
onready var _initial_transform: Transform2D = self.transform

var _origin: Vector2 = Vector2(0, 0)
var _rotation_angles = {}

func start_animating(frame_counter: FrameCounter):
	_randomize()
	_randomize()
	visible = false
	
	self._frame_counter = frame_counter
	
	self._frame_timer_lifetime = FrameTimer.new(self._frame_counter)
	self._frame_timer_lifetime.connect("timer_complete", self, "_on_frame_timer_lifetime_timer_complete")
	self._frame_timer_lifetime.start(self.lifetime)

func _on_frame_counter_counter_changed(new_num_frames, new_num_seconds):
	var frame_diff = new_num_frames - self._start_frame
	self._origin = self._velocity * frame_diff
	
	self.transform = self._initial_transform
	self.position += self._origin
	
	var full_angle = frame_diff * self.rotate_speed
	var size_of_segment = TAU / self.num_rotate_angles
	var num_segments = int((full_angle / size_of_segment)) % self.num_rotate_angles

	self.rotate(self._rotation_angles[num_segments])

func _on_frame_timer_lifetime_timer_complete():
	visible = true
	self._start_frame = self._frame_counter.num_frames
	self._frame_counter.connect("counter_changed", self, "_on_frame_counter_counter_changed")

func _randomize():
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	
	var rand_min = 0.50
	var rand_max = 1.50
	
	self.initial_velocity *= Vector2(rng.randf_range(rand_min * rand_min, rand_max * rand_max), rng.randf_range(rand_min * rand_min, rand_max * rand_max))
	self.rotate_speed *= rng.randf_range(rand_min, rand_max)
	self.num_rotate_angles = int(self.num_rotate_angles * rng.randf_range(rand_min, rand_max))
	
	self._rotation_angles = {}
	for angle in self.num_rotate_angles:
		self._rotation_angles[angle] = (TAU / self.num_rotate_angles) * angle
