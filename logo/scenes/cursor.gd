extends Node2D
const FrameTimer = preload("res://canon/classes/frame_timer.gd")

export(float) var blink_delay = 0.33

var _frame_counter: FrameCounter
var _frame_timer_blink: FrameTimer

func reset_cursor():
	$_.position.x = 0

func move_cursor():
	$_.position.x += 500

func hide_cursor():
	$_.visible = false

func start_blinking(frame_counter: FrameCounter):
	self._frame_counter = frame_counter
	self._frame_timer_blink = FrameTimer.new(self._frame_counter)
	self._frame_timer_blink.connect("timer_complete", self, "_on_frame_timer_blink_timer_complete")
	self._frame_timer_blink.start(self.blink_delay)

func stop_blinking():
	self._frame_timer_blink.stop()
	self._frame_timer_blink.disconnect("timer_complete", self, "_on_frame_timer_blink_timer_complete")

func _on_frame_timer_blink_timer_complete():
	$_.visible = !$_.visible
	self._frame_timer_blink.start(self.blink_delay)
