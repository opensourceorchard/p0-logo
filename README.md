# P0 - Logo

This repository contains the Godot code to generate the OSO logo animation.

[![Demo CountPages alpha](logo.gif)](https://www.youtube.com/watch?v=ROEmZkTesFc)



## External Resources...
https://github.com/hiulit/Godot-3-2D-CRT-Shader

https://github.com/Calinou/godot-video-rendering-demo

http://webdraft.hu/fonts/classic-console/
